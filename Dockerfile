ARG BASE_IMAGE="alpine:3.15"
ARG BUILD_IMAGE="kinsamanka/alpine-jtango:0.1"
FROM $BUILD_IMAGE as buildenv

ARG SCADA_VER=v2.7.1

USER root

# Install build time dependencies
RUN apk --update add --no-cache git gradle nodejs npm maven openjdk11

# build Scada-LTS
RUN git clone --depth=1 --branch=${SCADA_VER} https://github.com/kinsamanka/Scada-LTS.git /scadalts && \
    rm /scadalts/WebContent/WEB-INF/lib/JTangoCommons* && \
    rm /scadalts/WebContent/WEB-INF/lib/TangORB* && \
    cp /usr/local/java/JTangoCommons*jar /scadalts/WebContent/WEB-INF/lib && \
    cp /usr/local/java/TangORB*jar /scadalts/WebContent/WEB-INF/lib && \
    cd /scadalts/scadalts-ui && \
    npm install && \
    node build-config "${SCADA_VER}" \
        "$(date +%s)" \
        "${SCADA_VER}" \
        "$(git rev-parse HEAD)" \
        "$(hostname)" \
	"" && \
    npm run-script build && \
    cd ../WebContent/resources && \
    npm install && \
    cd ../../ && \
    gradle -PskipUi=true war

FROM $BASE_IMAGE

ARG BUILD_DATE
ARG GIT_REV
ARG DOCKER_TAG="0.2"

LABEL \
    org.opencontainers.image.title="scada_lts-tango" \
    org.opencontainers.image.description="Scada-LTS with TANGO datasource" \
    org.opencontainers.image.authors="GP Orcullo<kinsamanka@gmail.com>" \
    org.opencontainers.image.version=$DOCKER_TAG \
    org.opencontainers.image.url="https://hub.docker.com/repository/docker/kinsamanka/scada_lts-tango" \
    org.opencontainers.image.source="https://gitlab.com/kinsa13/tango/Docker/scada_lts-tango" \
    org.opencontainers.image.revision=$GIT_REV \
    org.opencontainers.image.created=$BUILD_DATE

ARG TOMCAT_VER=9.0.60
ARG MYSQL_DATABASE=mysql
ARG MYSQL_USER=scada
ARG MYSQL_PASSWORD=scada

COPY --from=buildenv /scadalts/build/libs/Scada-LTS.war /opt/tomcat/webapps.new/
COPY --from=buildenv /scadalts/WebContent/WEB-INF/lib/mysql-connector-java-5.1.49.jar /opt/tomcat/lib/
COPY --from=buildenv /scadalts/tomcat/lib/activation.jar /opt/tomcat/lib/
COPY --from=buildenv /scadalts/tomcat/lib/jaxb-api-2.4.0-b180830.0359.jar /opt/tomcat/lib/
COPY --from=buildenv /scadalts/tomcat/lib/jaxb-core-3.0.2.jar /opt/tomcat/lib/
COPY --from=buildenv /scadalts/tomcat/lib/jaxb-runtime-2.4.0-b180830.0438.jar /opt/tomcat/lib/
COPY --from=buildenv /scadalts/docker/config/context.xml /opt/tomcat/conf/context.xml.new
COPY --from=buildenv /usr/local/bin/wait-for-it /usr/local/bin/

RUN apk --update add --no-cache bash openjdk11-jre && \
    wget https://dlcdn.apache.org/tomcat/tomcat-9/v${TOMCAT_VER}/bin/apache-tomcat-${TOMCAT_VER}.tar.gz -O - | \
        tar xzf - --strip-components 1 --directory /opt/tomcat && \
    rm /opt/tomcat/webapps -rf && \
    mv /opt/tomcat/webapps.new /opt/tomcat/webapps && \
    mv /opt/tomcat/conf/context.xml.new /opt/tomcat/conf/context.xml && \
    sed -e "s/username=\"root\"/username=\"${MYSQL_USER}\"/g" \
        -e "s/password=\"root\"/password=\"${MYSQL_PASSWORD}\"/g" \
        -e "s/database/${MYSQL_DATABASE}/" \
        -i /opt/tomcat/conf/context.xml && \
    adduser -h /home/tango -s /bin/bash -D tango && \
    chown tango:tango -R /opt/tomcat

WORKDIR /opt/tomcat

USER tango
